# MediaTek AIoT Benchmark Suite

## Overview
This suite contains several popular benchmark tools collected by MediaTek for
customers to evaluate MediaTek AIoT platforms. Currently following tools are included:

* CPU
  * Whetstone: Floating-point performance benchmark
  * Dhrystone: Integer and string operation performance benchmark
  * Coremark: A modern benchmark tool implementing typical algorithms representing
    modern programs
  * c-ray: A simple ray tracer for benchmarking floating-point performance
* Memory
  * STREAM: Measuring memory bandwidth by using a large dataset
  * LMBench: A benchmark suite for measuring memory bandwidth and latency
  * perf memset/memcpy: Built-in benchmark of perf tool. Note this suite only includes
    a script for running the benchmark. The target system needs to have perf tool
    installed.
* GPU
  * glmark2: OpenGL 2.0 and openGLES 2.0 benchmark tool
* ML
  * benchmark_model: TensorFlow Lite model benchmark tool. Currently CPU, GPU and NNAPI
    delegates are supported. Note not every platform supports GPU or NNAPI delegates,
    so benchmark results may vary.
  * neuron_sdk: Neuron model benchmark tool. Note not every platform except Genio-350
    supports GPU or NNAPI delegates, so benchmark results may vary.

Those tools are publicly available and can be downloaded from 3rd-party websites listed
in `ORIGIN` file in each tool folder.

MediaTek didn't modify source code of benchmark, except modifications needed for passing
compilation, which are listed in `patches` folder in each tool folder.

The suite is designed with portability in mind; the user should be able to port the
suite to other platforms running embedded Linux easily.

## Requirements
To build the benchmark, the following softwares are needed:

* make
* meson (>= 0.47)
* CMake (>= 3.16)
* libpng
* libjpeg
* wayland for MediaTek AIoT platforms or X11 for x86 system
* libGL
* libEGL & libGLESv2
* perf (executable in runtime)
* numpy (>=1.22.3)

In addition, to build benchmark_model, an Internet connection is required to download
needed headers during the build process.

For MediaTek AIoT platforms, please install AIoT application SDK, which already contains
required softwares to build the suite. For other platforms, please consult the manual
of your system on how to install required packages.

## How to build
To build the benchmark, please checkout source code from GitLab repository:

    git clone https://gitlab.com/mediatek/aiot/bsp/benchmark_suite.git

You might need to input your user name and access token if necessary.

After cloning repository, change working folder to `benchmark_suite`, and edit `common` file to
fit your building environment.  After editing, run following command to start building process:

    ./build.sh

If the building process succeeds, built binaries and corresponding scripts are put under
`_build/benchmark_<ARCH>` folder, where each benchmark has its own subfolder.

In addition to running `build.sh` in the top-level folder, each benchmark can be built
individually:

    cd benchmark/dhrystone
    ./build.sh

### Cross-build with AIoT application SDK
To cross-build benchmarks with AIoT application SDK, please run following commands:

    source <SDK_INSTALL_PATH>/environment-setup-aarch64-poky-linux
    cd <BENCHMARK_SOURCE_DIR>
    # Edit common to adjust NUM_CORES
    ./build.sh

Note the number of CPU cores of your target system might not be the same as your host
system, please modify `NUM_CORES` variable in `common` to set correct value.

### Cross-build in general way
If you're using cross toolchain other than AIoT application SDK, please make sure:

* Export following environment variables:
  * CC: The C compiler in full path
  * CXX: The C++ compiler in full path
  * CROSS_COMPILE: The cross toolchain prefix, such as "aarch64-linux-gnu-"
  * CFLAGS: C compiler flags
  * CXXFLAGS: C++ compiler flags
* Edit `common` to modify settings matching with the target machine (
  especially `NUM_CORES`).
* All required softwares are installed (including libraries and headers) in the sysroot
  of the cross toolchain

## How to run
Copy built binaries (e.g. the folder under `_build`) to your target system, and run
the benchmark with specific number of round with following commands:

    cd <BENCHMARK_BINARY_DIR>
    ./run.sh --loop $NUM

For example, `./run.sh --loop 5` will run the overall benchmark 5 times.
The test result will show on the console with the information
`mean`, `standard deviation` and `coeffieient of variation` for each test item.

The output should look like this:

    Coremark 269973.85 iter/sec
    =============Statistic Evaluation Report================
    Coremark - iter
    Mean: 51208.74, STD: 5.46, C.V.: 0.0

    Coremark - score
    Mean: 12.96, STD: 0.0, C.V.: 0.0

    Dhrystone - DMIPS
    Mean: 24908.2, STD: 10.9, C.V.: 0.0

    Dhrystone - DMIPS/MHz
    Mean: 18.16, STD: 0.64, C.V.: 0.04

    c-ray - milliseconds
    Mean: 191.5, STD: 0.5, C.V.: 0.0

    ...