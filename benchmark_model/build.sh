#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/benchmark_model

unpack()
{
	if [ "$USE_PREBUILT_BINARIES" == "1" ]; then
		return 0
	fi

	tar zxf tensorflow-2.8.0.tar.gz || return 1
}

build()
{
	if [ "$USE_PREBUILT_BINARIES" == "1" ]; then
		return 0
	fi

	pushd tensorflow-2.8.0
	patch -p1 < ../patches/tflite.patch || return 1
	patch -p1 < ../patches/nnapi-fix.patch || return 1
	patch -p1 < ../patches/external-fix.patch || return 1

	mkdir build && cd build
	CMAKE_OPTS="-DTFLITE_ENABLE_NNAPI=on -DTFLITE_ENABLE_GPU=on -DTFLITE_ENABLE_XNNPACK=on"
	if [ "$ARCH" == "aarch64" ]; then
		CMAKE_OPTS="-DCMAKE_TOOLCHAIN_FILE=../tensorflow/lite/cmake/toolchain-yocto-mtk.cmake $CMAKE_OPTS"
	fi
	cmake $CMAKE_OPTS ../tensorflow/lite/ || return 1
	make -j8 benchmark_model
}

install()
{
	mkdir -p $INSTALL_DIR || return 1

	if [ "$USE_PREBUILT_BINARIES" == "1" ]; then
		cp -a bin/$ARCH/benchmark_model $INSTALL_DIR || return 1
	else
		cp -a tools/benchmark/benchmark_model $INSTALL_DIR || return 1
		popd
	fi

	cp -a mobilenet_v2_1.0_224_float.tflite run.sh $INSTALL_DIR || return 1
	cp -a mobilenet_v2_1.0_224_quant.tflite $INSTALL_DIR || return 1

}

cleanup()
{
	rm -rf tensorflow-2.8.0
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
