#!/bin/bash

BIN=benchmark_model
if [ ! -f "$BIN" ]; then
	echo -e "benchmark_model \033[31mFAIL\033[0m"
	exit 1
fi

# Detect delegates the platform supports
DELEGATES="cpu"

# GPU delegate
if ! ./$BIN --graph=$PWD/mobilenet_v2_1.0_224_quant.tflite --use_gpu=true --dry_run=true 2>&1 | grep -q Fail; then
	DELEGATES="$DELEGATES gpu"
fi

# NNAPI delegate
./$BIN --graph=$PWD/mobilenet_v2_1.0_224_quant.tflite --use_nnapi=true --dry_run=true > detect.log 2>&1
if ! grep -q "NNAPI acceleration is unsupported" detect.log; then
	DELEGATES="$DELEGATES nnapi"
fi

get_opts()
{
	case "$1" in
		cpu)
			;;
		gpu)
			echo "--use_gpu=true"
			;;
		nnapi)
			echo "--use_nnapi=true"
			;;
		*)
			;;
	esac
}
for f in $DELEGATES; do
	for m in *.tflite; do
		MODEL_NAME=${m%.tflite}
		echo -n "benchmark_model($MODEL_NAME, $f) "
		OPTS=`get_opts $f`
		./$BIN --graph=$PWD/$m $OPTS > run.$MODEL_NAME.$f.log 2>&1 || echo -e "\033[31mFAIL\033[0m"
		T=`cat run.$MODEL_NAME.$f.log | grep "Inference (avg)" | awk '{print $NF}'`
		MSEC=`bc <<< "scale=2; $T / 1000"`
		printf "%s ms\n" $MSEC
	done
done

