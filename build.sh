#!/bin/bash

SUBDIRS="coremark c-ray dhrystone lmbench perf_mem stream whetstone_sp glmark2 benchmark_model benchmark_model_214 neuron_sdk output"
FAILS=""

for s in $SUBDIRS; do
	echo -e "\033[32m================\033[0m"
	echo -e "\033[32mBuilding $s\033[0m"
	echo -e "\033[32m================\033[0m"
	cd $s
	./build.sh || FAILS="$FAILS $s"
	cd - > /dev/null 2>&1
done

if [ -n "$FAILS" ]; then
	echo -e "[\033[31mFAIL\033[0m] Failure happened on building: $FAILS"
	exit 1
fi

source common
if [ ! -n "$ARCH" ]; then
	ARCH=`uname -m`
fi
cp -a scripts/run.sh _build/benchmark_$ARCH
