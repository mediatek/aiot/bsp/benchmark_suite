#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/c-ray

unpack()
{
	tar zxf c-ray-1.1.tar.gz || return 1
}

build()
{
	pushd c-ray-1.1
	make CC="$CC"
	popd
}

install()
{
	mkdir -p $INSTALL_DIR || return 1
	cp -a run.sh c-ray-1.1/c-ray-mt c-ray-1.1/scene $INSTALL_DIR || return 1
}

cleanup()
{
	rm -rf c-ray-1.1
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
