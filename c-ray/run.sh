#!/bin/bash

echo -n "c-ray "

if [ ! -f c-ray-mt ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

./c-ray-mt -t 1 -i scene -o output.ppm > run.log 2>&1
cat run.log | grep "^Rendering" | sed 's/.*(\(.\+\))$/\1/'
