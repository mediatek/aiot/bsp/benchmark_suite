#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/coremark

unpack()
{
	unzip coremark-20190727.zip || return 1
	pushd coremark-master
}

build()
{
	# If NUM_CORES is not set in common, autodetect in host environment
	if [ -z "$NUM_CORES" ]; then
		NUM_CORES=`grep "^processor" /proc/cpuinfo | wc -l`
	fi
#make CC="$CC" XCFLAGS="$CFLAGS -O0 -DMULTITHREAD=$NUM_CORES -DUSE_FORK=1" compile || return 1
	make CC="$CC" XCFLAGS="$CFLAGS -DMULTITHREAD=$NUM_CORES -DUSE_FORK=1" compile || return 1
}

install()
{
	popd
	mkdir -p $INSTALL_DIR || return 1
	cp -a run.sh coremark-master/coremark.exe $INSTALL_DIR || return 1
}

cleanup()
{
	rm -rf coremark-master
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
