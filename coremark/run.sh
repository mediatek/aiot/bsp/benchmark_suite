#!/bin/bash

echo -n "Coremark "

if [ ! -f coremark.exe ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

./coremark.exe > run.log
#cat run.log | grep "^Iterations/Sec" | awk '{printf "%.02f iter/sec\n", $3}'

BASE="/sys/devices/system/cpu/cpufreq"
get_avg_mhz2()
{
	TOTAL=0
	COUNT=0
	POLICIES=`ls $BASE`

	for p in $POLICIES; do
		FREQ=`cat $BASE/$p/scaling_cur_freq`
		CPUS=`cat $BASE/$p/affected_cpus | wc -w`

		TOTAL=$(($TOTAL + $FREQ * $CPUS))
		COUNT=$(($COUNT + $CPUS))
	done

	echo $(($TOTAL / $COUNT))
}

CORES=`cat run.log | grep "Parallel Fork" | awk '{print $4}'`
SCORE=`cat run.log | grep "^Iterations/Sec" | awk '{print $3}'`
MHZ=`get_avg_mhz2`
MHZ=$(($MHZ / 1000))
SCORE_PER_MHZ=`bc <<< "scale=2; $SCORE / $MHZ / $CORES"`
printf "%.02f iter/sec\n" $SCORE
printf "Coremark %.02f score/MHz\n" $SCORE_PER_MHZ
