#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/dhrystone

unpack()
{
	return 0
}

build()
{
	make
}

install()
{
	mkdir -p $INSTALL_DIR || return 1
	cp -a run.sh dry2 $INSTALL_DIR || return 1
}

cleanup()
{
	make clean
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
