#!/bin/bash

echo -n "Dhrystone "

if [ ! -f dry2 ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

BASE="/sys/devices/system/cpu/cpufreq"
get_avg_mhz2()
{
	TOTAL=0
	COUNT=0
	POLICIES=`ls $BASE`

	for p in $POLICIES; do
		FREQ=`cat $BASE/$p/scaling_cur_freq`
		CPUS=`cat $BASE/$p/affected_cpus | wc -w`

		TOTAL=$(($TOTAL + $FREQ * $CPUS))
		COUNT=$(($COUNT + $CPUS))
	done

	echo $(($TOTAL / $COUNT))
}

#./dry2 2>/dev/null | grep "^Dhrystones per Second" | awk '{printf "%.02f DMIPS\n", $4 / 1757}'
./dry2 > run.log 2>&1
SCORE=`cat run.log | grep "^Dhrystones per Second" | awk '{print $4 / 1757}'`
MHZ=`get_avg_mhz2`
MHZ=$(($MHZ / 1000))
SCORE_PER_MHZ=`bc <<< "scale=2; $SCORE / $MHZ"`
printf "%.02f DMIPS\n" $SCORE
printf "Dhrystone %.02f DMIPS/MHz\n" $SCORE_PER_MHZ
