#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/glmark2

unpack()
{
	if [ "$USE_PREBUILT_BINARIES" == "1" ]; then
		return 0
	fi

	tar Jxf glmark2-20210830.tar.xz || return 1
}

build()
{
	if [ "$USE_PREBUILT_BINARIES" == "1" ]; then
		return 0
	fi

	pushd glmark2-master

	FLAVOR=x11-gl
	patch -p1 < ../patches/header-fix.patch || return 1
	if [ "$ARCH" == "aarch64" ]; then
		patch -p1 < ../patches/glmark2-fix.patch || return 1
		FLAVOR=wayland-glesv2,drm-glesv2
	fi

	meson build -Dflavors=$FLAVOR --buildtype=release || return 1
	ninja -C build || return 1
	DESTDIR=$PWD/out
	mkdir -p $DESTDIR
	DESTDIR=$DESTDIR ninja -C build install || return 1

	popd
}

install()
{
	mkdir -p $INSTALL_DIR/data || return 1

	if [ "$USE_PREBUILT_BINARIES" == "1" ]; then
		cp -a run.sh bin/$ARCH/glmark2* $INSTALL_DIR || return 1
		tar zxf data.tar.gz -C $INSTALL_DIR || return 1
	else
		OUT=$DESTDIR/usr/local
		cp -a run.sh $OUT/bin/glmark2* $INSTALL_DIR || return 1
		cp -a $OUT/share/glmark2 $INSTALL_DIR/data || return 1
	fi
}

cleanup()
{
	rm -rf glmark2-master
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
