#!/bin/bash

echo -n "glmark2 "

RESOLUTION="-s 1920x1080"

BIN=glmark2
for i in `ls`; do
	if echo $i | grep -q "^glmark2-es2-wayland"; then
		BIN=$i
	fi
done

if [ ! -f "$BIN" ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

./$BIN --data-path $PWD/data/glmark2 $RESOLUTION > run.log || exit 1
cat run.log | grep Score | awk '{printf("%d points\n", $3)}'
