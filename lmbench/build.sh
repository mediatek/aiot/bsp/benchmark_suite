#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/lmbench

get_os()
{
	# For aarch64 build host which can not be recognized
	# by the lmbench build script, give it a default value.
	local dir="bin"
	for d in bin/*; do
		if [ ! -d "$d" ]; then
			continue
		fi
		if [ "$d" == "." ] || [ "$d" == ".." ]; then
			continue
		fi

		dir=$d
	done

	echo $dir
}

unpack()
{
	tar zxf lmbench-2.5.tgz || return 1
}

build()
{
	pushd lmbench-2.5
	patch -p 1 < ../patches/lmbench-fix.patch || return 1
	patch -p 1 < ../patches/lmbench-fix-2.patch || return 1
	TEMP_CC=`echo $CC | sed 's/-Werror[[:graph:]]\+//'`
	make CC="$TEMP_CC" || return 1
}

install()
{
	mkdir -p $INSTALL_DIR || return 1
	cp `get_os`/lat_mem_rd $INSTALL_DIR || return 1
	popd
	cp run.sh $INSTALL_DIR || return 1
}

cleanup()
{
	rm -rf lmbench-2.5
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
