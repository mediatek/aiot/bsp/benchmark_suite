#!/bin/bash

echo -n "LMBench_L1 "

if [ ! -f lat_mem_rd ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

./lat_mem_rd 8 128 > run.log 2>&1

if cat run.log | grep -q "^0.00098"; then
	L1_LAT=`cat run.log | grep "^0.00098" | awk '{print $2}'`
fi

if cat run.log | grep -q "^0.12500"; then
	L2_LAT=`cat run.log | grep "^0.12500" | awk '{print $2}'`
fi

if [ -z "$L1_LAT" ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
else
	echo "$L1_LAT ns"
fi

echo -n "LMBench_L2 "
if [ -z "$L2_LAT" ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
else
	echo "$L2_LAT ns"
fi
