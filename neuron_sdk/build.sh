#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/neuron_sdk

unpack()
{
	return 0
}

build()
{
	return 0
}

install()
{
	mkdir -p $INSTALL_DIR || return 1
	cp -a run.sh $INSTALL_DIR || return 1
}

cleanup()
{
	return 0
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
