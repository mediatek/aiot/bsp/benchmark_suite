#!/bin/bash

SOC=(`cat /proc/device-tree/compatible | cut -d',' -f2 | cut -d'-' -f1`)

VPU_TARGET="vpu"
MDLA_TARGET="mdla3.0"

if [ "$SOC" == "mt8395" ] ; then
	VPU_TARGET="vpu_fpu"
	MDLA_TARGET="mdla2.0"
fi


rm -rf run.log

for m in ../benchmark_model/*.tflite; do
	python3 /usr/share/benchmark_dla/benchmark.py --target $MDLA_TARGET --file $m  > /dev/null 2>&1 >> run.log
	python3 /usr/share/benchmark_dla/benchmark.py --target $VPU_TARGET --file $m  > /dev/null 2>&1 >> run.log
done

for m in  ../benchmark_model/*.tflite; do
	MODEL_NAME=(`echo ${m%.tflite} | sed 's/..\/benchmark_model\///'`)

	VPU_T=`cat run.log | grep $MODEL_NAME.tflite | grep $VPU_TARGET | awk '{print $NF}'`
	if [ ! -z "$VPU_T" ] ; then
		echo -n "benchmark_model($MODEL_NAME, $VPU_TARGET) "
		echo -e "$VPU_T ms"
	fi
	
	MDLA_T=`cat run.log | grep $MODEL_NAME.tflite | grep $MDLA_TARGET | awk '{print $NF}'`	
	if [ ! -z "$MDLA_T" ] ; then
		echo -n "benchmark_model($MODEL_NAME, $MDLA_TARGET) "
		echo -e "$MDLA_T ms"
	fi
done
