#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/output

unpack()
{
	return 0
}

build()
{
	return 0
}

install()
{
	mkdir -p $INSTALL_DIR || return 1
	cp -a parse_output.py $INSTALL_DIR || return 1
	cp -a task_list_g350.txt $INSTALL_DIR || return 1
	cp -a task_list_others.txt $INSTALL_DIR || return 1
}

cleanup()
{
	return 0
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
