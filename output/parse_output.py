#!/usr/bin/env python3

import subprocess
import numpy as np
import argparse
import json
import os, sys

output_data = []

def runcmd(command):
    ret = subprocess.run(command,shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding="utf-8")
    
    return ret

def calculate(task):
    ret = runcmd(["awk '" + task + "' " + args.input + " | rev | cut -d' ' -f2 | rev"])
    if ret.stdout.split('\n')[0] == "":
        ret_mean = "skip"
        ret_std = "skip"
        ret_cv = "skip"
        return ret_mean, ret_std, ret_cv

    ret_flt = np.asarray(ret.stdout.split(), dtype=float)
    ret_mean = round(np.mean(ret_flt),2)
    ret_std = round(np.std(ret_flt),2)
    ret_cv = round(ret_std/ret_mean,2)

    return ret_mean, ret_std, ret_cv

def get_soc_name():
    ret = subprocess.run(["cat /proc/device-tree/compatible"],shell=True,stdout=subprocess.PIPE,stderr=subprocess.PIPE,encoding="utf-8")
    soc = ret.stdout.split(',')[1].split('-')[0]
    return soc

def detect_mach():
    if os.path.isfile("/proc/device-tree/compatible"):
        return get_soc_name()
    else:
        try:
            out = subprocess.check_output("uname -m", shell=True)
            return out.decode('utf-8').rstrip('\n')
        except subprocess.CalledProcessError:
            return None

    return None

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument(
        '-i',
        '--input',
        help='benchmark statistics to be computed')
    parser.add_argument(
        '-t',
        '--task',
        help='task list in the benchmark statistics')
    args = parser.parse_args()

    soc = detect_mach()

    print("=============Statistic Evaluation Report================")
    with open(args.task) as file:
        for line in file:   
            benchmark_model_mean, benchmark_model_std, benchmark_model_cv = calculate(line.split('\n')[0])
            line_split = line.split('\n')[0].split('&&')
            
            task = line_split[0].split('/')[1]
            if len(line_split) > 2:
                task = line_split[0].split('/')[1] + '_' + line_split[1].split('/')[1]

            unit = line_split[-1].split('$')[0].split('/')[1]
            if unit[-1] == "\\":
                unit = unit.split("\\")[0] + "/" + line_split[-1].split('$')[0].split('/')[2]

            print("{0} - {1}".format(task, unit))
            print("Mean: {0}, STD: {1}, C.V.: {2}\n".format(benchmark_model_mean, benchmark_model_std, benchmark_model_cv))

            output_result = {
                    "task": task,
                    "unit": unit,
                    "mean": benchmark_model_mean,
                    "std": benchmark_model_std,
                    "cv": benchmark_model_cv
            }
            output_data.append(output_result)

    output_json = {
        "num_of_runs": int(args.input.split('_')[1]) ,
        "test_result": "pass",
        "test_data": output_data
    }

            
    with open('output.json', 'w') as json_file:
        json.dump(output_json, json_file, indent=4)
