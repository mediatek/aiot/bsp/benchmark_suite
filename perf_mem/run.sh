#!/bin/bash

echo -n "Memset "

if ! which perf > /dev/null 2>&1; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

if ! perf bench mem memset -s 1000MB > run.log; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi
cat run.log | sed '/^#\|^$/d' | head -n1 | awk '{printf "%.02f %s\n", $1, $2}'

echo -n "Memcpy "

if ! perf bench mem memcpy -s 500MB > run.log; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi
cat run.log | sed '/^#\|^$/d' | head -n1 | awk '{printf "%.02f %s\n", $1, $2}'
