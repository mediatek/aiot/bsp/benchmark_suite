#!/bin/bash

detect_mach()
{
	if [ -f "/proc/device-tree/compatible" ]; then
		local SOC=(`cat /proc/device-tree/compatible | cut -d',' -f2 | cut -d'-' -f1`)
		echo $SOC
	else
		echo `uname -m`
	fi
}

MACH=`detect_mach`
SUBDIRS="coremark dhrystone c-ray stream lmbench whetstone perf_mem glmark2 benchmark_model"
TASKLIST="task_list_g350.txt"

echo "Detected machine: $MACH"
if echo $MACH | grep -q "mt*"; then
	# For Genio platforms other than mt8365, add neuron_sdk test item
	if [ "$MACH" != "mt8365" ] ; then
		SUBDIRS+=" neuron_sdk"
		TASKLIST="task_list_others.txt"
	fi
else
	# For other platforms (e.g. x86_64)
	TASKLIST="task_list_others.txt"
fi

LOGDIR=$PWD/output

while [[ $# -gt 0 ]]; do
	case "$1" in
		--loop)
			loop=$2
			shift 2
			;;
		--help|-h)
			echo "Usage: $0 --loop [lp]"
			echo ""
			echo "Run the benchmark test"
			echo ""
			echo "Options:"
			echo " --loop			 Repeat the benchmark test with specific number of loop (default: 1)"
			exit 0
			;;
		*)
			break
			;;
	esac
done

# Remove dirty things
rm -rf ${LOGDIR}/*.log

if [ -z "${loop}" ]; then
	loop=1
fi

round=0
echo "Note: It will take about 12 mins to finish a round!"
 
FAILED_ITEMS=""
while [ $round -lt $loop ] ; do
	echo "--- Round : $((round+1)) ---" >> ${LOGDIR}/benchmark_${loop}_round.log
	echo "--- Round : $((round+1)) ---"

	for s in $SUBDIRS; do
		cd $s
		echo "Run test ${s}..."
		if ! ./run.sh >> ${LOGDIR}/benchmark_${loop}_round.log; then
			HAS_FAIL=1
			FAILED_ITEMS="$FAILED_ITEMS $s"
		fi
		cd - > /dev/null 2>&1
	done

	if [ -n "$HAS_FAIL" ]; then
		echo -e "\033[31m[FAIL]\033[0m Fail to complete following test items:"
		echo "  $FAILED_ITEMS"
		exit 1
	fi
	round=$(($round+1))
done
echo "--------Finished--------"

cd output
python3 parse_output.py -i benchmark_${loop}_round.log -t ${TASKLIST}
