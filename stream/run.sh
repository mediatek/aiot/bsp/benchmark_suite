#!/bin/bash

echo -n "STREAM "

if [ ! -f stream ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

./stream > run.log
cat run.log | grep "^Copy" | awk '{printf "%.02f MB/s\n", $2}'
