#!/bin/bash

source ../common

INSTALL_DIR=$INSTALL_BASE/whetstone

unpack()
{
	return 0
}

build()
{
	make
}

install()
{
	mkdir -p $INSTALL_DIR || return 1
	cp -a run.sh whets $INSTALL_DIR || return 1
}

cleanup()
{
	make clean
}

##########
#  MAIN  #
##########

unpack || exit 1
build || exit 1
install || exit 1
cleanup
