#!/bin/bash

echo -n "Whetstone "

if [ ! -f whets ]; then
	echo -e "\033[31mFAIL\033[0m"
	exit 1
fi

./whets | grep "^MWIPS" | awk '{printf "%.02f MWIPS\n", $2}'
